#!usr/bin/python

"""
Before running this script, make sure that you have sqlite3 
installed in your user area. You can check this by typing "$sqlite3 --version"
at the command line

Once the sqlite3 is installed, check that you are in the directory where you
wish the database to be created. at the command prompt, enter "sqlite3 nameofdatabase.db"

This should create the database. You can check this by typing ".databases"
after the sqlite3> prompt. It should display the information about the database created.

Then, type ".quit" to exit the database.

To create the flagstat database, you can use the script by typing "python flagstat_db.py nameofdatabase.db".
"""

import os
import sys
import sqlite3

def connect_flagstat_db(db_file):

#Create a connection to the SQLite database specified by the db_file

    db_dir = os.path.dirname(db_file)

    try:
        db = sqlite3.connect(db_file)
        #print "connection to sqlite3 established"
        return db

    except Exception as err:
        print (err)
        with open(db_dir+'/connerrlog.txt', 'a') as connerrlog:
            connerrlog.write('Exception occurred: %s' % err)

def execute_sql(db, sqlstatement):

    try:
        run = db.cursor()
        result = run.execute(sqlstatement)
        #for thing in result:
            #print (thing)
        # don't need to commit, error message:"'sqlite3.Cursor' object has no attribute 'commit'"
        db.commit()
        return result

    except Exception as err:
        print (err)
        with open('/mnt/storage/home/zhengt/djangoproject/djangoproject2/flagstat/flagstat_db/exerrlog.txt', 'a') as exerrlog:
            exerrlog.write('Exception occurred: %s' % err)
        raise

"""Functions to create tables for flagstat data, 
sid = sample_ID, rfid = runfolder_ID, srfid = sample_runfolder_ID"""

def create_sample_table(db):

    action = """
    CREATE TABLE IF NOT EXISTS sample (id INTEGER PRIMARY KEY AUTOINCREMENT, 
    sample_name TEXT NOT NULL,
    in_total_passed INT NOT NULL, 
    in_total_failed INT NOT NULL, 
    duplicates_passed INT NOT NULL, 
    duplicates_failed INT NOT NULL, 
    mapped_passed INT NOT NULL, mapped_failed INT NOT NULL, 
    paired_in_sequencing_passed INT NOT NULL, 
    paired_in_sequencing_failed INT NOT NULL, 
    read1_passed INT NOT NULL, 
    read1_failed INT NOT NULL, 
    read2_passed INT NOT NULL, 
    read2_failed INT NOT NULL, 
    properly_paired_passed INT NOT NULL, 
    properly_paired_failed INT NOT NULL, 
    with_itself_and_mate_mapped_passed INT NOT NULL, 
    with_itself_and_mate_mapped_failed INT NOT NULL, 
    singletons_passed INT NOT NULL, 
    singletons_failed INT NOT NULL, 
    with_mate_mapped_to_a_different_chr_passed INT NOT NULL, 
    with_mate_mapped_to_a_different_chr_failed INT NOT NULL, 
    with_mate_mapped_to_a_different_chr_mapq5_passed INT NOT NULL, 
    with_mate_mapped_to_a_different_chr_mapq5_failed INT NOT NULL, 
    runfolder_id INTEGER NOT NULL, FOREIGN KEY (runfolder_id) REFERENCES runfolder(id))"""

    execute_sql(db, action)

def create_runfolder_table(db):

    action = """
    CREATE TABLE IF NOT EXISTS runfolder (id INTEGER PRIMARY KEY AUTOINCREMENT, runfolder_name TEXT NOT NULL,
    in_total_passed INT NOT NULL, in_total_failed INT NOT NULL, 
    duplicates_passed INT NOT NULL, 
    duplicates_failed INT NOT NULL, 
    mapped_passed INT NOT NULL, mapped_failed INT NOT NULL, 
    paired_in_sequencing_passed INT NOT NULL, 
    paired_in_sequencing_failed INT NOT NULL, 
    read1_passed INT NOT NULL, 
    read1_failed INT NOT NULL, 
    read2_passed INT NOT NULL, 
    read2_failed INT NOT NULL, 
    properly_paired_passed INT NOT NULL, 
    properly_paired_failed INT NOT NULL, 
    with_itself_and_mate_mapped_passed INT NOT NULL, 
    with_itself_and_mate_mapped_failed INT NOT NULL, 
    singletons_passed INT NOT NULL, 
    singletons_failed INT NOT NULL, 
    with_mate_mapped_to_a_different_chr_passed INT NOT NULL, 
    with_mate_mapped_to_a_different_chr_failed INT NOT NULL, 
    with_mate_mapped_to_a_different_chr_mapq5_passed INT NOT NULL, 
    with_mate_mapped_to_a_different_chr_mapq5_failed INT NOT NULL)"""

    execute_sql(db, action)

def main(db_file):

    #refer the connect_flagstat_db as db in other functions
    db = connect_flagstat_db(db_file)

    # function calls to create tables in the database
    create_sample_table(db)

    create_runfolder_table(db)
    
    create_parameters_table(db)

    #specify the name of the db to create it on command line
    
if __name__ == "__main__":
    main(sys.argv[1])


