# Flagstat database
  - By Tengyue Zheng
  - 09/01/2017 

## Main scripts:
  - flagstat_2.py
  - flagstat_db_2.py
  - flagstat_import2.py
  - flagstat_import_runfolder.py
  - flagstat_import_all.py

## Function:
  Flagstat database creator using Python 3.4.5

  To clone Git repository go here: https://github.com/tz2614/flagstat_db

  The artificial flagstat files initially used for testing is stored in the Git repository under the directory /flagstat_test_files.

## User Requirements:
  inspect flagstat data files from the ```python mnt/storage/data/NGS``` and store flagstat parameter information, along with its associated sample name and runfolder name into a flagstat database ```python nameofyourdatabase.db```.

## User Stories:
  - Input flagstat file or runfolder.
  - Input logic tests to extracts information from flagstat files ONLY.
  - Store relevant information including samples, runfolder, parameters in the relevant tables in the flagstat database. 

## Tasks:

To test the scripts in flagstat_database follow the instruction below:

1. Create a database that allows flagstat data for each sample to be imported from runfolder.

Navigate to the ```mnt/storage/home/zhengt/djangoproject/djangoproject2/flagstat/flagstat_db directory```

Before running this script, make sure that you have sqlite3 installed in your user area. 
You can check this by typing ```sqlite3 --version```at the command line

Enter the following commands from terminal to create the database.
```python
python flagstat_db_2.py nameofdatabase.db
```
To check that the database is created, at the command prompt, enter ```sqlite3 nameofdatabase.db```

then enter 
```.databases``` 

It should display the information about the database created.

Then, type ```.quit``` or ```.exit```  to exit the database.
see flagstat_db_2.py


2. Create a script that uses the dictionary from 2. and imports data from a single flagstat file.

To import a single flagstat file into the database, check that you are in the directory ```mnt/storage/home/zhengt/djangoproject/djangoproject2/flagstat/flagstat_db directory```, if not navigate to it.

From command line enter 
```python 
python flagstat_import2.py nameofrunfolder/stats/nameofflagstat.flagstat nameofdatabase.db
```
To check that the flagstat has been imported, enter ```sqlite3 nameofdatabase.db```

then enter 
```.header on``` to show column headings for each table
then enter
```SELECT nameofflagstat FROM sample;``` It should display the information of all samples imported.
then enter
```SELECT nameofrunfolder FROM runfolder;``` It should display the information of the runfolder imported.

Then, type ```.quit``` or ```.exit``` to exit the database.
see flagstat_import2.py

3. Create a script that imports data from all flagstat files from a single runfolder into a database.

To import a single runfolder into the database, check that you are in the directory ```mnt/storage/home/zhengt/djangoproject/djangoproject2/flagstat/flagstat_db directory```, if not navigate to it.

From command line enter 
```python 
python flagstat_import_runfolder.py nameofrunfolder/ nameofdatabase.db
```

To check that the flagstat information from runfolder has been imported, enter ```sqlite3 nameofdatabase.db```

then enter 
```.header on``` to show column headings for each table
then enter 
```SELECT nameofrunfolder FROM runfolder;``` It should display the information of the runfolder imported.
make note of the ```id``` number for the runfolder
then enter
```SELECT * FROM sample WHERE runfolder_id=id;``` It should display the information of all samples imported.

Then, type ```.quit``` or ```.exit``` to exit the database.
see flagstat_import_runfolder.py for additional information

4. Create a script that imports data from all flagstat files from NGS folder into a database.

To import all flagstat from NGS folder into the database, check that you are in the directory ```mnt/storage/home/zhengt/djangoproject/djangoproject2/flagstat/flagstat_db directory```, if not navigate to it.

From command line enter 
```python 
python flagstat_import_all.py mnt/storage/data/NGS/ nameofdatabase.db
```

To check that the flagstat information from NGS folder has been imported, enter ```sqlite3 nameofdatabase.db```

then enter 
```.header on``` to show column headings for each table
then enter 
```SELECT * FROM runfolder;``` It should display the information of all runfolder imported, make note of the ```id``` number for the runfolder

then enter
```SELECT * FROM sample;``` It should display the information of all the samples imported.

Then, type ```.quit``` or ```.exit``` to exit the database.

see flagstat_import_all.py for additional information

- Please see flagstat_db.png in the repository for the schematic of the database.
