#!usr/bin/python

"""
Create a function to extract vcf.qc parameters from a sample vcf.qc 
file into a dictionary.

"""

import sys
import pprint
import sqlite3

def data_from_vcfqc (vcf_qc_file):

        with open(sample_sheet, 'r') as csv:
            while not line.startswith("[Data]"):
            line = csv.readline()
            print line
            for index, line in enumerate(csv.readlines()):
                if index == 0:
                    column_headings = line.split(",")
                    pos = column_headings.index("Sample_ID")
                    print pos
                else:
                    sample_name2 = line.split(",")[pos]
                    print sample_name2
                    if sample_name1 == sample_name2:
                        sample_list.append(sample_name2)

    vcfqcdict = {}
    with open (vcf_qc_file) as file_handle:
        for count, line in enumerate (file_handle.readlines()):
            # strip out the new lines at the end of each line.
            if count == 0:
                parameters = line.strip().split(" ")
            if count == 1:
                data = line.strip().split(" ")
        vcfqcdict = dict(zip(parameters, data))
    pprint.pprint (vcfqcdict)
    return vcfqcdict

def main(filepath):
    # from command line enter "python", followed by the filepath of the vcf.qc file you wish to create a dictionary for.
    # Call the function to print the dict to terminal.
    data_from_vcfqc(filepath)

if __name__ == "__main__":
    main(sys.argv[1])
