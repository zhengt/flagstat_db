#!usr/bin/python

#from __future__ import print_function
import sys
import flagstat_2
import flagstat_db_2
import os
import datetime
import time
import pprint

"""
Import all flagstat data from a single flagstat file to a specified database.
"""

def import_sample_to_db (db_dir, conn, sample_name, runfolder_id, flagdict):

    # check the flagdict is not empty, if empty it means the flagstat files are empty, record the sample name.
    if not flagdict:
        print ("the flagstat for this sample: {} is not available".format(sample_name))
        with open(os.path.join(db_dir,'flagstat_err_list.txt'), 'a') as flagstat_err_list:
            flagstat_err_list.writelines('flagstat data for {} is not available \n'.format(sample_name))
            return flagdict

    else:        
        str_sample_name = '"'+sample_name+'"'
        print ("sample: {}".format(str_sample_name))
        pprint.pprint(flagdict)

        # check each flagstat parameter is present in teh dictionary before importing the data into database.

        if "in_total" not in flagdict.keys():
            print ('{}.flagstat have incorrect in total parameters'.format(sample_name))
            with open(os.path.join(db_dir,'flagstat_parameters_checklist.txt'), 'a') as flagstat_check_list:
                flagstat_check_list.writelines('{}.flagstat have incorrect in total parameters\n'.format(sample_name))

        elif "duplicates" not in flagdict.keys():
            print ('{}.flagstat have incorrect duplicates parameters'.format(sample_name))
            with open(os.path.join(db_dir,'flagstat_parameters_checklist.txt'), 'a') as flagstat_check_list:
                flagstat_check_list.writelines('{}.flagstat have incorrect duplicates parameters\n'.format(sample_name))

        elif "mapped" not in flagdict.keys():
            print ('{}.flagstat have incorrect mapped parameters'.format(sample_name))
            with open(os.path.join(db_dir,'flagstat_parameters_checklist.txt'), 'a') as flagstat_check_list:
                flagstat_check_list.writelines('{}.flagstat have incorrect mapped parameters\n'.format(sample_name))

        """ The following are additional checks for other parameters from the flagstat not required for the current NGS QC database.

        elif "paired_in_sequencing" not in flagdict.keys():
            print ('{}.flagstat have incorrect paired in sequencing parameters'.format(sample_name))
            with open(os.path.join(db_dir,'flagstat_parameters_checklist.txt'), 'a') as flagstat_check_list:
                flagstat_check_list.writelines('{}.flagstat have incorrect paired in sequencing parameters\n'.format(sample_name))

        elif "read1" not in flagdict.keys():
            print ('{}.flagstat have incorrect read1 parameters'.format(sample_name))
            with open(os.path.join(db_dir,'flagstat_parameters_checklist.txt'), 'a') as flagstat_check_list:
                flagstat_check_list.writelines('{}.flagstat have incorrect parameters\n'.format(sample_name))

        elif "read2" not in flagdict.keys():
            print ('{}.flagstat have incorrect read2 parameters'.format(sample_name))
            with open(os.path.join(db_dir,'flagstat_parameters_checklist.txt'), 'a') as flagstat_check_list:
                flagstat_check_list.writelines('{}.flagstat have incorrect read2 parameters\n'.format(sample_name))

        elif "properly_paired" not in flagdict.keys():
            print ('{}.flagstat have incorrect properly paired parameters'.format(sample_name))
            with open(os.path.join(db_dir,'flagstat_parameters_checklist.txt'), 'a') as flagstat_check_list:
                flagstat_check_list.writelines('{}.flagstat have incorrect properly paired parameters\n'.format(sample_name))

        elif "with_itself_and_mate_mapped" not in flagdict.keys():
            print ('{}.flagstat have incorrect with itself and mate mapped parameters'.format(sample_name))
            with open(os.path.join(db_dir,'flagstat_parameters_checklist.txt'), 'a') as flagstat_check_list:
                flagstat_check_list.writelines('{}.flagstat have incorrect with itself and mate mapped parameters\n'.format(sample_name))

        elif "singletons" not in flagdict.keys():
            print ('{}.flagstat have incorrect singletons parameters'.format(sample_name)))
            with open(os.path.join(db_dir,'flagstat_parameters_checklist.txt'), 'a') as flagstat_check_list:
                flagstat_check_list.writelines('{}.flagstat have incorrect singletons parameters\n'.format(sample_name))

        elif "with_mate_mapped_to_a_different_chr" not in flagdict.keys():
            print ('{}.flagstat have incorrect with mate mapped to a different chr parameters'.format(sample_name))
            with open(os.path.join(db_dir,'flagstat_parameters_checklist.txt'), 'a') as flagstat_check_list:
                flagstat_check_list.writelines('{}.flagstat have incorrect with mate mapped to a different chr parameters\n'.format(sample_name))

        elif "with_mate_mapped_to_a_different_chr_MapQ5" not in flagdict.keys():
            print ('{}.flagstat have incorrect with mate mapped to a different chr MapQ5 parameters'.format(sample_name))
            with open(os.path.join(db_dir,'flagstat_parameters_checklist.txt'), 'a') as flagstat_check_list:
                flagstat_check_list.writelines('{}.flagstat have incorrect with mate mapped to a different chr MapQ5 parameters\n'.format(sample_name))"""

        column_headings = flagdict.keys()
        new_column_headings = ["sample_name"]
        new_column_values = [str_sample_name]
        
        for heading in column_headings:
            #print (heading)
            for status in ["passed"]: #if needed we can add this parameter later, "failed"
                column = heading + "_" + status
                value = flagdict[heading][status]
                new_column_headings.append(column)
                new_column_values.append(value)

        new_column_headings.append("runfolder_id")
        new_column_values.append(str(runfolder_id))
        #print ("new_column_headings:", new_column_headings)
        #print ("new_column_values:", new_column_values)
        table_columns = ', '.join(new_column_headings)
        table_values = ', '.join(new_column_values)
        
        sqlstatement1 = "INSERT INTO sample ({}) SELECT {} WHERE NOT EXISTS (SELECT 1 FROM sample WHERE sample_name = {} AND runfolder_id = {})".format(table_columns, table_values, str_sample_name, runfolder_id)
        flagstat_db_2.execute_sql(db_dir, conn, sqlstatement1)

        sqlstatement2 = "UPDATE sample SET import_date=datetime('now') WHERE sample_name={} AND runfolder_id={}".format(str_sample_name, runfolder_id)
        flagstat_db_2.execute_sql(db_dir, conn, sqlstatement2)
        print ("sample data for {} inserted into sample table".format(str_sample_name))

def import_runfolder_to_db (db_dir, conn, runfolder_name, flagdict):

    # check the flagdict is not empty, if empty it means the flagstat files are empty, record the runfolder name.
    if not flagdict:
        print ("the flagstat of a sample in this runfolder: {} is not available".format(runfolder_name))
        with open(os.path.join(db_dir,'flagstat_err_list.txt'), 'a') as flagstat_err_list:
            flagstat_err_list.writelines('runfolder_name: {} \n'.format(runfolder_name))
            return flagdict
    else:
        str_runfolder_name = '"'+runfolder_name+'"'
        print ("runfolder: {}".format(str_runfolder_name))

        sqlstatement1 = "INSERT INTO runfolder (runfolder_name) SELECT ({}) WHERE NOT EXISTS (SELECT runfolder_name FROM runfolder WHERE runfolder_name = {})".format(str_runfolder_name, str_runfolder_name)
        """insert runfolder_name into runfolder database"""
        flagstat_db_2.execute_sql(db_dir, conn, sqlstatement1)
        print ("runfolder {} inserted into runfolder table".format(str_runfolder_name))

def get_runfolder_id (db_dir, conn, runfolder_name, flagdict):

    if not flagdict:
        return flagdict
    else:
        str_runfolder_name = '"'+runfolder_name+'"'

        """check if there are multiple runfolders with the same name or not, if runfolder is unique return runfolder id"""
        sqlstatement2 = "SELECT id FROM runfolder WHERE runfolder_name = {}".format(str_runfolder_name)
        
        runfolder_id = flagstat_db_2.execute_sql(db_dir, conn, sqlstatement2)
        runfolder_id = [x[0] for x in runfolder_id]
        print ("runfolder_id: ", runfolder_id)

        assert len(runfolder_id) == 1
        runfolder_id = runfolder_id[0]
        return runfolder_id
    
def import_runfolder_flagstat_to_db (db_dir, conn, runfolder_id, flagdict):
    
    # Take the sum of flagstat values from sample table containing the same runfolder_id, and insert them into runfolder table.
    if not flagdict:
        return flagdict
    else:
        sqlstatement1 = "SELECT in_total_passed FROM sample WHERE runfolder_id = {}".format(runfolder_id)
        in_total_passed = flagstat_db_2.execute_sql(db_dir, conn, sqlstatement1)
        in_total_passed = [x[0] for x in in_total_passed]
        for num in in_total_passed:
            assert str(num).isdigit()
        in_total_passed = sum(in_total_passed)
        print("total_reads:", in_total_passed)

        sqlstatement2 = "SELECT mapped_passed FROM sample WHERE runfolder_id = {}".format(runfolder_id)
        mapped_passed = flagstat_db_2.execute_sql(db_dir, conn, sqlstatement2)
        mapped_passed = [x[0] for x in mapped_passed]
        for num in mapped_passed:
            assert str(num).isdigit()
        mapped_passed = sum(mapped_passed)
        print("mapped_reads:", mapped_passed)

        sqlstatement3 = "SELECT duplicates_passed FROM sample WHERE runfolder_id = {}".format(runfolder_id)
        duplicates_passed = flagstat_db_2.execute_sql(db_dir, conn, sqlstatement3)
        duplicates_passed = [x[0] for x in duplicates_passed]
        for num in duplicates_passed:
            assert str(num).isdigit()
        duplicates_passed = sum(duplicates_passed)
        print("duplicate_reads:", duplicates_passed)

        sqlstatement4 = "UPDATE runfolder SET import_date=datetime('now') WHERE id={}".format(runfolder_id)
        import_date = flagstat_db_2.execute_sql(db_dir, conn, sqlstatement4)
        flagstat_db_2.execute_sql(db_dir, conn, sqlstatement4)
        
        sqlstatement5 = "UPDATE runfolder SET total_reads={}, mapped_reads={}, duplicate_reads={} WHERE id={}".format(in_total_passed, mapped_passed, duplicates_passed, runfolder_id)
        flagstat_db_2.execute_sql(db_dir, conn, sqlstatement5)
        print ("total_reads, mapped_reads, duplicate_reads inserted into runfolder table")

def database_dir(db_file):
    #provide directory to where database is stored
    db_dir = os.path.dirname(os.path.abspath(db_file))
    return db_dir

def main(flagstatfile, db_file):

    start = time.time()
    filepath = os.path.abspath(flagstatfile)

    db_dir = database_dir(db_file)

    #assign flagstat dictionary to a variable
    flagdict = flagstat_2.data_from_flagstat(db_dir, flagstatfile)

    #refer the connect_flagstat_db as db in other functions
    conn = flagstat_db_2.connect_flagstat_db(db_dir, db_file)

    #specify the name of runfolder
    runfolder_name = filepath.split("/")[-3]

    #specify the name of the sample
    sample_name = filepath.split("/")[-1].split(".")[0]

    import_runfolder_to_db(db_dir, conn, runfolder_name, flagdict)
    runfolder_id = get_runfolder_id(db_dir, conn, runfolder_name, flagdict)
    #print (runfolder_id)
    import_sample_to_db(db_dir, conn, sample_name, runfolder_id, flagdict)
    import_runfolder_flagstat_to_db (db_dir, conn, runfolder_id, flagdict)

    end = time.time()
    time_taken = end - start
    with open(os.path.join(db_dir, 'time.txt'), 'a') as time_text:
            time_text.writelines("time taken to import {} from {} : {} \n".format(sample_name, runfolder_name, time_taken))
    
if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
    
"""from command line enter "python", and then the path to the script you wish to run e.g. flagstat_import2.py, 
followed by the file name of the flagstat file you wish to import - sys.argv[1] 
and then specify the name of the database to import the data into - sys.argv[2]"""
