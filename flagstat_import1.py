#!usr/bin/python

from __future__ import print_function
import sys
import pprint
import sqlite3
import flagstat
import flagstat_db
import os

"""
Import all flagstat data from a single flagstat file to a specified database.
"""

def import_sample_to_db (db, sample_name):
    
    str_sample_name = '"'+sample_name+'"'
    
    sqlstatement1 = "INSERT INTO sample (sample_name) SELECT ({}) WHERE NOT EXISTS (SELECT sample_name FROM sample WHERE sample_name = {})".format(str_sample_name, str_sample_name)
    
    flagstat_db.execute_sql(db, sqlstatement1)

    sqlstatement2 = "SELECT id FROM sample WHERE sample_name = {}".format(str_sample_name)
    
    result = flagstat_db.execute_sql(db, sqlstatement2)
    #print (result)
    result = [x[0] for x in result]
    #print (result)

    #check returns one item from sid list.
    assert len(result) == 1
    sid = result[0]

    #sqlstatement = "SELECT SCOPE_IDENTITY()" This is MySQL statement. sqlite3 uses sqlite3.connect(db_file).execute(sqlstatement).lastrowid 
    #print ("sample_name inserted into sample table")

    return sid

def import_runfolder_to_db (db, rf_name):

    str_rf_name = '"'+runfolder_name+'"'
    
    sqlstatement1 = "INSERT INTO runfolder (runfolder_name) SELECT ({}) WHERE NOT EXISTS (SELECT runfolder_name FROM runfolder WHERE runfolder_name = {})".format(str_rf_name, str_rf_name)
    
    flagstat_db.execute_sql(db, sqlstatement1)

    sqlstatement2 = "SELECT id FROM runfolder WHERE runfolder_name = {}".format(str_runfolder_name)
    
    result = flagstat_db.execute_sql(db, sqlstatement2)
    #print (result)
    result = [x[0] for x in result]
    #print (result)

    #check returns one item from rfid list.
    assert len(result) == 1
    rfid = result[0]
    
    #sqlstatement = "SELECT SCOPE_IDENTITY()""
    #print ("rf_name inserted into runfolder table!")
    
    return rfid

#Need to think about how to pull runfolder id and sample id from file names.
def import_sample_runfolder_to_db (db, sid, rfid):

    sqlstatement1 = "INSERT INTO sample_runfolder (id, rfid) SELECT {}, {} WHERE NOT EXISTS (SELECT 1 FROM sample_runfolder WHERE sid = {} AND rfid = {})".format(sid, rfid, sid, rfid)
    flagstat_db.execute_sql(db, sqlstatement1)

    #print ("sid, rfid imported into sample_runfolder table")
    
    sqlstatement2 = "SELECT srfid FROM sample_runfolder WHERE sid = {} AND rfid = {}".format(sid, rfid)
    result = flagstat_db.execute_sql(db, sqlstatement2)
    #print ("TESTING", srfid)
    #for thing in srfid:
        #print ("SRFID: ", (thing))

    #print (result)
    result = [x[0] for x in result]
    #print (result)
    
    #check returns one item from srfid list.
    assert len(result) == 1
    srfid = result[0]

    #sqlstatement = "SELECT SCOPE_IDENTITY()"
    #srfid = flagstat_db.execute_sql(db_file, db, sqlstatement)

    #print ("srfid imported into sample_runfolder table")

    return srfid

def import_parameters_to_db (db, flagdict, srfid):
    column_headings = flagdict.keys()
    new_column_headings = []
    new_column_values = []
    
    for heading in column_headings:
        #print (heading)
        for status in ["passed", "failed"]:
            column = heading + "_" + status
            value = flagdict[heading][status]
            new_column_headings.append(column)
            new_column_values.append(value)
    #print ("new_column_headings:", new_column_headings)
    #print ("new_column_values:", new_column_values)
    new_column_headings.append("srfid")
    table_columns = ', '.join(new_column_headings)
    new_column_values.append(str(srfid))
    table_values = ', '.join(new_column_values)
    
    sqlstatement = "INSERT INTO parameters ({}) SELECT {} WHERE NOT EXISTS (SELECT 1 FROM parameters WHERE srfid = {})".format(table_columns, table_values, srfid)
    
    flagstat_db.execute_sql(db, sqlstatement)
    #print ("parameters inserted into table!")


def main(flagstatfile, db_file):

    filepath = os.path.abspath(flagstatfile)

    #assign flagstat dictionary to a variable
    flagdict = flagstat.data_from_flagstat(flagstatfile)

    #refer the connect_flagstat_db as db in other functions
    db = flagstat_db.connect_flagstat_db(db_file)

    #specify the name of runfolder
    rf_name = filepath.split("/")[-3]
    print ("runfolder: ", rf_name)

    #specify the name of the sample
    sample_name = filepath.split("/")[-1].split(".")[0]
    print ("sample name: ", sample_name)

    #insert sample_names into sample table
    sid = import_sample_to_db(db, sample_name)

    #insert rf_names into runfolder table
    rfid = import_runfolder_to_db(db, rf_name)

    #import sid, rfid to sample_runfolder and generate srfid
    srfid = import_sample_runfolder_to_db (db, sid, rfid)

    #import parameters into flagstat database.
    import_parameters_to_db (db, flagdict, srfid)

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
    
#from command line enter "python", followed by the file name of the flagstat file you wish to create a dictionary for - sys.argv[1]

#specify the name of the database to create it on command line - sys.argv[2]


