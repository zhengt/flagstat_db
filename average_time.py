#!usr/bin/python

"""Create a function to calculate the average processing time for importing flagstat data into a sqlite3 database."""

import sys
import pprint as pp
import sqlite3
import os

def time_flagstat (timefile, db_dir):

    times = {}
    per_flagstat_times = []
    per_runfolder_times = []
    all_runfolders_times = []
    database_create_times = []

    with open(os.path.join(db_dir, 'average_time.txt'), 'w+') as timetext:
        timetext.write("")

    with open (timefile) as file_handle:
        for line in file_handle:
            # strip out the spaces at the end of each line.
            fields = line.strip().split(" ")
            if "from" in fields:
                per_flagstat_times.append(fields[-1])
                        
            if "runfolder" in fields:
                per_runfolder_times.append(fields[-1])
                        
            if "all" in fields and "data" in fields:
                with open(os.path.join(db_dir, 'average_time.txt'), 'a+') as timetext:
                    timetext.writelines("time to import all runfolders: {}\n".format(fields[-1]))
                all_runfolders_times.append(fields[-1])

            if "create" in fields and "database" in fields:
                database_create_times.append(fields[-1])

    #print (per_flagstat_times)
    #print (per_runfolder_times)
    #print (all_runfolders_times)

    per_flagstat_time = sum([float(x) for x in per_runfolder_times])/len(per_flagstat_times)
    per_runfolder_time = sum([float(x) for x in per_runfolder_times])/len(per_runfolder_times)
    all_runfolders_time = sum([float(x) for x in all_runfolders_times])/len(all_runfolders_times)
    database_create_time = sum([float(x) for x in database_create_times])/len(database_create_times)

    with open(os.path.join(db_dir, 'average_time.txt'), 'a+') as timetext:
        timetext.writelines("number of flagstat imported: {}\n average time taken to import one flagstat: {}\n".format(len(per_flagstat_times), per_flagstat_time))
        timetext.writelines("number of runfolders imported: {}\n average time taken to import each runfolder: {}\n".format(len(per_runfolder_times), per_runfolder_time))
        timetext.writelines("number of times all runfolders imported: {}\n average time taken to import all runfolders: {}\n".format(len(all_runfolders_times), all_runfolders_time))
        timetext.writelines("number of times database were created: {}\n average time taken to create a database: {}\n".format(len(database_create_times), database_create_time))

    times["a_flagstat"] = per_flagstat_time
    times["a_runfolder"] = per_runfolder_time
    times["all_runfolders"] = all_runfolders_time
    times["a_database"] = database_create_time
    
    for key, value in sorted(times.items()):
        print (key, value)
    
    return times

def main(timefile):

    db_dir = os.path.dirname(os.path.abspath(timefile))

    # call the function to print the number to terminal and save them in average_time.txt
    time_flagstat(timefile, db_dir)

if __name__ == "__main__":
    main(sys.argv[1])

"""From terminal navigate to the directory where the script is stored, 
    
    enter "python average_time.py", followed by the filepath to the time.txt file to calculate the average time taken to import flagstat data
    e.g. /mnt/storage/home/zhengt/djangoproject/djangoproject2/flagstat_db/time.txt"""
