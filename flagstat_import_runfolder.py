#!usr/bin/python

#from __future__ import print_function
import sys
import glob
import flagstat_2
import flagstat_db_2
import flagstat_import2
import os
import time

"""
Parse all flagstat files from the runfolder into a database.
"""

def list_of_files_to_import(runfolder):
    
    #identify all flagstat files in the runfolder and assign it to flagstat_list
    filepath = os.path.abspath(runfolder)
    pattern = "{}/stats/*.flagstat".format(str(filepath))
    #print (pattern)

    #generate a list of filepath to the flagstat files in the runfolder. 
    flagstat_list = glob.glob(pattern)
    flagstat_list = sorted(flagstat_list)

    return flagstat_list

def get_sample_sheet(runfolder, db_dir):
    
    #identify samplesheet*.csv with all the samples listed, use it to check that flagstat_list contains all the samples in the runfolder
    sample_sheet = "{}/SampleSheet*.csv".format(str(runfolder))
    sample_sheets = sorted(glob.glob(sample_sheet))
    if sample_sheets:
        return sample_sheets[0]
    else:
        print ("no SampleSheet*.csv found")
        with open(os.path.join(db_dir, 'no_samples_check.txt'), 'a') as sample_check_log:
            sample_check_log.writelines("no SampleSheet*.csv found in {}\n".format(filepath))
    
def check_files_to_be_imported(flagstat_list, sample_sheet, db_dir, runfolder):
    
    flagstat_sample_name_list = []
    sample_sheet_sample_name_list = []
    new_flagstat_list = []
    count = 0

    """create a list of sample_names with flagstat files"""
    for flagstat in flagstat_list:
        sample_name1 = "".join(flagstat.split("/")[-1].split(".")[:1])
        #print (sample_name1)
        flagstat_sample_name_list.append(sample_name1)
    #print(flagstat_sample_name_list)

    """create a list of sample_names in the SampleSheet.csv"""
    if sample_sheet:
        print ("SampleSheet present at: {}".format(sample_sheet))
        with open(os.path.join(db_dir, 'yes_samples_check.txt'), 'a') as sample_check_log:
            sample_check_log.writelines("SampleSheet present at: {}\n".format(sample_sheet))
        with open(sample_sheet, 'r') as csv:
            
            heading_line = False
            count = 0
            
            for line in csv:
                
                if line.startswith("[Data]"):
                    heading_line = True

                elif heading_line and count == 0:
                    line = line.strip()
                    column_headings = line.split(",")
                    #print (column_headings)
                    pos = column_headings.index('Sample_ID')
                    count += 1
                        
                elif heading_line and count >= 1:
                    line = line.strip()
                    #print (line)
                    sample_name2 = line.split(",")[pos]
                    #print (sample_name2)
                    sample_sheet_sample_name_list.append(sample_name2)
                    count += 1

        #print (sample_sheet_sample_name_list)
        #print (flagstat_sample_name_list)
        samples_list1 = set(sample_sheet_sample_name_list)
        print (len(samples_list1))
        samples_list2 = set(flagstat_sample_name_list)
        print (len(samples_list2))
        unmatched_samples = samples_list1 ^ samples_list2

        if unmatched_samples:
            runfolder_name = os.path.abspath(runfolder).split("/")[-1]
            with open(os.path.join(db_dir, 'samples_check.txt'), 'a') as sample_check_log:
                sample_check_log.writelines("The runfolder for the sample sheet is: {}\n".format(runfolder_name))

        # the following cross-checks the list of unmatched samples in both list and records them in the sample_check.txt

        for sample in unmatched_samples:
            if sample in flagstat_sample_name_list:
                print ("{} not in SampleSheet.csv, check {}".format(sample, sample))
                with open(os.path.join(db_dir, 'samples_check.txt'), 'a') as sample_check_log:
                    sample_check_log.writelines("{} not in SampleSheet.csv, check {}\n".format(sample, sample))

            elif sample in sample_sheet_sample_name_list:
                print ("{} not in stats/*.flagstat_list, check {}.flagstat".format(sample, sample))
                with open(os.path.join(db_dir, 'samples_check.txt'), 'a') as sample_check_log:
                    sample_check_log.writelines("{} not in flagstat_list, check {}.flagstat\n".format(sample, sample))

        else:
            matched_samples = set(sample_sheet_sample_name_list) & set(flagstat_sample_name_list)
            final_sample_list = list(matched_samples)
            #print (sample_sheet_sample_name_list)
            #print (flagstat_sample_name_list)

            if final_sample_list == []:
                print ("flagstat files checked against SampleSheet.csv, but no matches found, use unchecked flagstat_list")
                with open (os.path.join(db_dir, 'samples_check.txt'), 'a') as sample_check_log:
                    sample_check_log.writelines("flagstat files checked against SampleSheet.csv, but no matches found, use unchecked flagstat_list\n")
                return flagstat_list
            else:
                for sample in final_sample_list:
                    for flagstat in flagstat_list:
                        if sample in flagstat:
                            new_flagstat_list.append(flagstat)
            print ("flagstat files checked against SampleSheet.csv")
            #print (new_flagstat_list)
            return new_flagstat_list
    else:
        return flagstat_list

def main(runfolder, db_file):

    start = time.time()

    db_dir = flagstat_import2.database_dir(db_file)
    print ("directory where database is stored:", db_dir)
    conn = flagstat_db_2.connect_flagstat_db(db_dir, db_file)

    # generate two lists of flagstat fles and compare to create a final list from runfolder to be imported
    flagstat_list = list_of_files_to_import(runfolder)
    sample_sheet = get_sample_sheet(runfolder, db_dir)
    processed_flagstat_list = check_files_to_be_imported(flagstat_list, sample_sheet, db_dir, runfolder)
    exit()
    # execute function to import all flagstat from runfolder into flagstat database

    for flagstat in processed_flagstat_list:
        flagstat_import2.main(flagstat, db_file)

    runfolder_path = os.path.abspath(runfolder)
    runfolder_name = runfolder_path.split("/")[-1]
    print ("runfolder: ", runfolder_name)
    
    # import runfolder stats to runfolder table
    flagdict = flagstat_2.data_from_flagstat(db_dir, flagstat_list[0])
    runfolder_id = flagstat_import2.get_runfolder_id(db_dir, conn, runfolder_name, flagdict)
    flagstat_import2.import_runfolder_flagstat_to_db (db_dir, conn, runfolder_id, flagdict)

    end = time.time()
    time_taken = end - start
    with open(os.path.join(db_dir, 'time.txt'), 'a') as time_text:
        time_text.writelines("time taken to import runfolder {}: {} \n".format(runfolder_name, time_taken))

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
    
"""From terminal navigate to the directory where the script is stored, 
    
    enter "python flagstat_import_runfolder.py", followed by the filepath of the runfolder you wish to import into the database - sys.argv[1] 
    e.g. /mnt/storage/data/NGS/180613_K00178

    and then the filepath of the database where the runfolder data will be imported - sys.argv[2] 
    e.g. gemini_db.db"""

