#!usr/bin/python

"""
Create a function to extract flagstatdict parameters from a sample flagstatdict 
file into a dictionary.

"""

import sys
import pprint
import sqlite3

def data_from_flagstat (flagstatfile):

    flagstatdict = {}
    with open (flagstatfile) as file_handle:
        for count, line in enumerate (file_handle):
            # strip out the new lines at the end of each line.
            fields = line.strip().split(" ")
            if count == 0:
                in_total = {}
                for field in fields:
                    if field.isdigit():
                        in_total["passed"] = fields[0]
                        in_total["failed"] = fields[2]
                flagstatdict["in_total"] = in_total
            if count == 1:
                duplicates = {}
                for field in fields:
                    if field.isdigit():
                        duplicates["passed"] = fields[0]
                        duplicates["failed"] = fields[2]
                flagstatdict["duplicates"] = duplicates
            if count == 2:
                mapped = {}
                for field in fields:
                    if field.isdigit():
                        mapped["passed"] = fields[0]
                        mapped["failed"] = fields[2]
                flagstatdict["mapped"] = mapped
            if count == 3:
                paired_in_sequencing = {}
                for field in fields:
                    if field.isdigit():
                        paired_in_sequencing["passed"] = fields[0]
                        paired_in_sequencing["failed"] = fields[2]
                flagstatdict["paired_in_sequencing"] = paired_in_sequencing 
            if count == 4:
                read1 = {}
                for field in fields:
                    if field.isdigit():
                        read1["passed"] = fields[0]
                        read1["failed"] = fields[2]
                flagstatdict["read1"] = read1
            if count == 5:
                read2 = {}
                for field in fields:
                    if field.isdigit():
                        read2["passed"] = fields[0]
                        read2["failed"] = fields[2]
                flagstatdict["read2"] = read2
            if count == 6:
                properly_paired = {}
                for field in fields:
                    if field.isdigit():
                        properly_paired["passed"] = fields[0]
                        properly_paired["failed"] = fields[2]
                flagstatdict["properly_paired"] = properly_paired
            if count == 7:
                with_itself_and_mate_mapped = {}
                for field in fields:
                    if field.isdigit():
                        with_itself_and_mate_mapped["passed"] = fields[0]
                        with_itself_and_mate_mapped["failed"] = fields[2]
                flagstatdict["with_itself_and_mate_mapped"] = with_itself_and_mate_mapped
            if count == 8:
                singletons = {}
                for field in fields:
                    if field.isdigit():
                        singletons["passed"] = fields[0]
                        singletons["failed"] = fields[2]
                flagstatdict["singletons"] = singletons
            if count == 9:
                with_mate_mapped_to_a_different_chr = {}
                for field in fields:
                    if field.isdigit():
                        with_mate_mapped_to_a_different_chr["passed"] = fields[0]
                        with_mate_mapped_to_a_different_chr["failed"] = fields[2]
                flagstatdict["with_mate_mapped_to_a_different_chr"] = with_mate_mapped_to_a_different_chr
            if count == 10:
                with_mate_mapped_to_a_different_chr_MapQ5 = {}
                for field in fields:
                    if field.isdigit():
                        with_mate_mapped_to_a_different_chr_MapQ5["passed"] = fields[0]
                        with_mate_mapped_to_a_different_chr_MapQ5["failed"] = fields[2]
                flagstatdict["with_mate_mapped_to_a_different_chr_MapQ5"] = with_mate_mapped_to_a_different_chr_MapQ5
    pprint.pprint (flagstatdict)
    return flagstatdict

def main(filepath):
    # from command line enter "python", followed by the filepath of the flagstat file you wish to create a dictionary for.
    # Call the function to print the dict to terminal.
    data_from_flagstat(filepath)

if __name__ == "__main__":
    main(sys.argv[1])

