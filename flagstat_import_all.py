#!usr/bin/python

#from __future__ import print_function
import sys
import pprint
import glob
import flagstat_import2
import flagstat_import_runfolder
import re
import os
import time

"""
Import all flagstat in the Gemini database
"""

def import_gemini_flagstat(geminifolder, db_file, db_dir):

    #identify all flagstat files in the gemini folder and assign it to flagstat_list
    
    #1. apply a regular expression pattern matching to identify all runfolders that contain the gemini runs.
    #2. place all runfolder into a list using the glob function.
    #3. iterate through the list to find all flagstat files belonging to the gemini runfolders, and import each flagstat file data into a database
    assert (geminifolder == "/mnt/storage/data/NGS" or geminifolder == "/mnt/storage/data/NGS/"), "runfolder given incorrect"
    patterns = "{}/[0-9][0-9][0-9][0-9][0-9][0-9]_*/stats/*.flagstat".format(str(geminifolder))

    #generate a list of filepath to the flagstat files in the geminifolder. 
    flagstat_list = glob.glob(patterns)
    flagstat_list = sorted(flagstat_list)
    #print (flagstat_list)
    runfolder_list = []

    for flagstat in flagstat_list:

        if re.search(r'/stats/', flagstat):
            filepath = os.path.abspath(flagstat)
            runfolder_name = os.path.dirname(os.path.dirname(filepath))
            
            if runfolder_name not in runfolder_list:
                runfolder_list.append(runfolder_name)
        else:
            print ("no flagstat files found within /stats/ folders for:", flagstat)

    #print (runfolder_list)
    for runfolder in runfolder_list:
        print ("importing:", runfolder)
        flagstat_import_runfolder.main(runfolder, db_file)
  
def main(geminifolder, db_file):
    
    start = time.time()

    db_dir = flagstat_import2.database_dir(db_file)
    print ("directory where database is stored:", db_dir)

    #execute function to import all flagstat data into flagstat database
    import_gemini_flagstat(geminifolder, db_file, db_dir)
    print ("import of flagstat data from gemini runfolders complete")

    end = time.time()
    time_taken = end - start
    with open(os.path.join(db_dir, 'time.txt'), 'a') as time_text:
            time_text.writelines("time taken to import all data from gemini runfolders : {} \n".format(time_taken))

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
    
"""From terminal navigate to the directory where the script is stored, 
    
    enter "python flagstat_import_all.py", followed by the filepath of the all the runfolders you wish to import into the database - sys.argv[1] 
    e.g. /mnt/storage/data/NGS/

    and then the filepath of the database where the runfolder data will be imported - sys.argv[2] 
    e.g. gemini_db.db"""