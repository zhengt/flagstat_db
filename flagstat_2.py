#!usr/bin/python

"""
Create a function to extract flagstatdict parameters from a sample flagstatdict 
file into a dictionary.

"""

import sys
import pprint
import sqlite3
import os
import flagstat_import2

def data_from_flagstat(db_dir, flagstatfile):

    flagstatdict = {}
    with open (flagstatfile) as file_handle:
        for line in file_handle:
            # strip out the new lines at the end of each line.
            fields = line.strip().split(" ")
            if fields[0].isdigit() and fields[1] == "+" and fields[2].isdigit():
                in_total = ["in", "total", "(QC-passed", "QC-failed", "reads", "reads)"]
                if all(field in fields for field in in_total):
                    in_total = {}
                    if fields[0].isdigit():
                        in_total["passed"] = fields[0]
                    #if fields[2].isdigit():
                        #in_total["failed"] = fields[2]
                        flagstatdict["in_total"] = in_total

                duplicates = ["duplicates"]
                if all(field in fields for field in duplicates):
                    duplicates = {}
                    if fields[0].isdigit():
                        duplicates["passed"] = fields[0]
                    #if fields[2].isdigit()
                        #duplicates["failed"] = fields[2]
                        flagstatdict["duplicates"] = duplicates
                
                mapped = ["mapped"]
                if fields[3] in mapped and len(fields) <= 7:
                    mapped = {}
                    if fields[0].isdigit():
                        mapped["passed"] = fields[0]
                    #if fields[2].isdigit():
                        #mapped["failed"] = fields[2]
                        flagstatdict["mapped"] = mapped

                """ The following parameters from the flagstat are not required for the current NGS QC database.
                
                paired_sequencing = ["paired", "in", "sequencing"]
                if all(field in fields for field in paired_sequencing):
                    paired_in_sequencing = {}
                    if fields[0].isdigit():
                        paired_in_sequencing["passed"] = fields[0]
                    if fields[2].isdigit():
                        paired_in_sequencing["failed"] = fields[2]
                        flagstatdict["paired_in_sequencing"] = paired_in_sequencing

                read1 = ["read1"]
                if all(fields in fields for field in read1):
                    read1 = {}
                    if fields[0].isdigit():
                        read1["passed"] = fields[0]
                    if fields[2].isdigit():
                        read1["failed"] = fields[2]
                        flagstatdict["read1"] = read1

                read2 = ["read2"]
                if all(fields in fields for field in read2):
                    read2 = {}
                    if fields[0].isdigit():
                        read2["passed"] = fields[0]
                    if fields[2].isdigit():
                        read2["failed"] = fields[2]
                        flagstatdict["read2"] = read2

                properly_paired = ["properly", "paired"]
                if all(field in fields for field in properly_paired):
                    properly_paired = {}
                    if fields[0].isdigit():
                        properly_paired["passed"] = fields[0]
                    if fields[2].isdigit():
                        properly_paired["failed"] = fields[2]
                        flagstatdict["properly_paired"] = properly_paired

                with_itself_and_mate_mapped = ["with", "itself", "and", "mate", "mapped"]
                if all(field in fields for field in with_itself_and_mate_mapped):
                    with_itself_and_mate_mapped = {}
                        if fields[0].isdigit():
                            with_itself_and_mate_mapped["passed"] = fields[0]
                        if fields[2].isdigit():
                            with_itself_and_mate_mapped["failed"] = fields[2]
                        flagstatdict["with_itself_and_mate_mapped"] = with_itself_and_mate_mapped

                singletons = ["singletons"]
                if all(field in fields for field in singletons):
                    singletons = {}
                    if fields[0].isdigit():
                        singletons["passed"] = fields[0]
                    if fields[2].isdigit():
                        singletons["failed"] = fields[2]
                        flagstatdict["singletons"] = singletons

                with_mate_mapped_to_a_different_chr = ["with", "mate", "mapped", "to", "a", "different", "chr"]
                if all(field in fields for field in with_mate_mapped_to_a_different_chr):
                    with_mate_mapped_to_a_different_chr = {}
                        if fields[0].isdigit():
                            with_mate_mapped_to_a_different_chr["passed"] = fields[0]
                        if fields[2].isdigit():
                            with_mate_mapped_to_a_different_chr["failed"] = fields[2]
                        flagstatdict["with_mate_mapped_to_a_different_chr"] = with_mate_mapped_to_a_different_chr

                with_mate_mapped_to_a_different_chr_mapQ5 = ["with", "mate", "mapped", "to", "a", "different", "chr", "(mapQ>=5)"]
                if all(field in fields for field in with_mate_mapped_to_a_different_chr_mapq5):
                    with_mate_mapped_to_a_different_chr_MapQ5 = {}
                        if fields[0].isdigit():
                            with_mate_mapped_to_a_different_chr_mapQ5["passed"] = fields[0]
                        if fields[2].isdigit():
                            with_mate_mapped_to_a_different_chr_mapQ5["failed"] = fields[2]
                        flagstatdict["with_mate_mapped_to_a_different_chr_mapQ5"] = with_mate_mapped_to_a_different_chr_mapQ5"""
            else:
                print ('{} do not start with # + #'.format(os.path.abspath(flagstatfile)))
                with open(os.path.join(db_dir,'flagstat_startwithvalues_checklist.txt'), 'a') as flagstat_check_list:
                    flagstat_check_list.writelines('{} do not start with # + #\n'.format(os.path.abspath(flagstatfile)))
    #pprint.pprint (flagstatdict)
    return flagstatdict

def main(filepath, db_file):

    """call the function to return the dictionary, and record the errors in the flagstat file"""
    data_from_flagstat(db_dir, filepath)
    db_dir = flagstat_import2.database_dir(db_file)
    

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])

    """From terminal navigate to the directory where the script is stored, 

    and enter "python flagstat_2.py"

    followed by the filepath of the flagstat file you wish to create a dictionary for - sys.argv[1]."""
    
