#!usr/bin/python

"""
Before running this script, make sure that you have sqlite3 
installed in your user area. You can check this by typing "$sqlite3 --version"
at the command line

Once the sqlite3 is installed, check that you are in the directory where you
wish the database to be created. at the command prompt, enter "sqlite3 nameofdatabase.db"

This should create the database. You can check this by typing ".databases"
after the sqlite3> prompt. It should display the information about the database created.

Then, type ".quit" to exit the database.

To create the flagstat database, you can use the script by typing "python flagstat_db_2.py nameofdatabase.db".
"""

import os
import sys
import sqlite3
import time

def connect_flagstat_db(db_dir, db_file):

#Create a connection to the SQLite database specified by the db_file

    #db_dir = os.path.dirname(db)
    try:
        conn = sqlite3.connect(db_file)
        #print "connection to sqlite3 established"
        return conn

    except Exception as err:
        print (err)
        with open(os.path.join(db_dir, 'connerrlog2.txt'), 'a') as connerrlog:
            connerrlog.writelines('Exception occurred: {} \n'.format(err))

def execute_sql(db_dir, conn, sqlstatement):

    #print sqlstatement

    try:
        result = conn.cursor().execute(sqlstatement)

        conn.commit()
        return result

    except Exception as err:
        print (err)
        with open(os.path.join(db_dir,'exerrlog2.txt'), 'a') as exerrlog:
            exerrlog.writelines('Exception occurred: {} \n'.format(err))
        raise

"""Functions to create tables for flagstat data"""

def create_sample_table(db_dir, conn):

    print ("creating sample table in", db_dir)
    print ("connecting to db:", conn)

    action = """
    CREATE TABLE IF NOT EXISTS sample (id INTEGER PRIMARY KEY AUTOINCREMENT, 
    sample_name TEXT NOT NULL, in_total_passed INT NOT NULL, mapped_passed INT NOT NULL, duplicates_passed INT NOT NULL,
    import_date TEXT, runfolder_id INT NOT NULL, CONSTRAINT fk_runfolder FOREIGN KEY (runfolder_id) REFERENCES runfolder(id))"""
    #in_total_failed INT NOT NULL, duplicates_failed INT NOT NULL, mapped_failed INT NOT NULL
    
    execute_sql(db_dir, conn, action)

def create_runfolder_table(db_dir, conn):

    print ("creating runfolder table in", db_dir)
    print ("connecting to db", conn)

    action = """
    CREATE TABLE IF NOT EXISTS runfolder (id INTEGER PRIMARY KEY AUTOINCREMENT, 
    runfolder_name TEXT NOT NULL, total_reads INT, mapped_reads INT, duplicate_reads INT, import_date TEXT)"""
    #in_total_failed INT NOT NULL, duplicates_failed INT NOT NULL, mapped_failed INT NOT NULL
    
    execute_sql(db_dir, conn, action)

def main(db_file):

    start = time.clock()

    #provide directory to where database is stored
    db_path = os.path.abspath(db_file)
    db_dir = os.path.dirname(db_path)
    #print "path to the directory: {}".format(db_dir)

    #refer the connect_flagstat_db as db in other functions
    conn = connect_flagstat_db(db_dir, db_file)
    #print db

    # function calls to create tables in the database
    create_sample_table(db_dir, conn)

    create_runfolder_table(db_dir, conn)
    end = time.clock()
    time_taken = end - start
    with open(os.path.join(db_dir, 'time.txt'), 'a') as time_text:
            time_text.writelines("time taken to create the database : {} \n".format(time_taken))


if __name__ == "__main__":
    
    main(sys.argv[1])
    
    
    """from terminal navigate to the directory where the script is stored,
    
    enter "python flagstat_db_2.py"
    
    followed by the filepath to the database you wish to create- sys.argv[1]"""



